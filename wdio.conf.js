const { generate } = require('multiple-cucumber-html-reporter');
const { removeSync } = require('fs-extra');
const maxInstances = process.env.MAX_INSTANCES || 5;

exports.config = {
  baseUrl: 'https://www.westpac.co.nz',
  specs: [`./src/features/**/*.feature`],
  exclude: [],
  maxInstances: maxInstances,
  capabilities: [
    {
      browserName: 'chrome',
      'goog:chromeOptions': {
        args: ['--start-maximized']
      },
      maxInstances
    }
  ],
  logLevel: 'error',
  bail: 0,
  waitforTimeout: 60000,
  connectionRetryTimeout: 90000,
  connectionRetryCount: 3,
  services: ['selenium-standalone'],
  framework: 'cucumber',
  reporters: [
    'spec',
    [
      'cucumberjs-json',
      {
        jsonFolder: 'tmp/json/',
        language: 'en'
      }
    ]
  ],

  cucumberOpts: {
    backtrace: true, // <boolean> show full backtrace for errors
    dryRun: false, // <boolean> invoke formatters without executing steps
    failFast: false, // <boolean> abort the run on first failure
    format: ['pretty'], // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
    colors: true, // <boolean> disable colors in formatter output
    snippets: true, // <boolean> hide step definition snippets for pending steps
    source: true, // <boolean> hide source uris
    profile: [], // <string[]> (name) specify the profile to use
    strict: false, // <boolean> fail if there are any undefined or pending steps
    tagExpression: '', // <string> (expression) only execute the features or scenarios with tags matching the expression
    timeout: 60000, // <number> timeout for step definitions
    ignoreUndefinedDefinitions: false, // <boolean> Enable this config to treat undefined definitions as warnings
    requireModule: [
      // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
      'tsconfig-paths/register',
      () => {
        require('ts-node').register({ files: true });
      }
    ],
    require: [
      './src/common/steps/*.ts',
      `./src/steps/*.ts`
    ]
  },
  onPrepare: () => {
    // Remove the `.tmp/` folder that holds the json and report files
    removeSync('tmp/');
  },
  onComplete: () => {
    // Generate the report when it all tests are done
    generate({
      jsonDir: 'tmp/json/',
      reportPath: 'tmp/report/'
    });
  }
};
