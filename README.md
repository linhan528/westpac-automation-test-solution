# Westpac automation test - KiwiSaver Retirement Calculator

The test is developed in [TypeScript](https://www.typescriptlang.org/) with [WebDriverIO V6](http://webdriver.io/) and [Cucumber](https://cucumber.io/)

## Prerequisite

- node >= 12.18.x - [how to install Node](https://nodejs.org/en/download/)
- npm >= 6.14.x - [how to install NPM](https://www.npmjs.com/get-npm)
- Java >= 1.8 - [Download Java runtime](https://www.java.com/en/download/)
- Python >= 3.7 - [Download Python](https://www.python.org/downloads/release/python-374/) (Make sure tick the option 'Add Python to environment variables' when installing)
- windows-build-tools - download it here: https://github.com/nodejs/node-gyp#on-windows, or using `npm install --global windows-build-tools` from an elevated PowerShell or CMD.exe (run as Administrator).
- Chrome - latest Chrome

## IDE

The test was developed using [Visual Studio Code](https://code.visualstudio.com/)

## Getting Started

#### The test run via command line, make sure you are at the root folder.

Install the dependencies:

```bash
npm install
```

Run the tests:

```bash
npm run test
```

## Reports

### [Multiple Cucumber HTML Reporter](https://github.com/wswebcreation/multiple-cucumber-html-reporter)

The report is automatically generated to directory `./tmp/report` every time the test finishes running.

![alt text][multiplecucumberhtmlreporter]

[multiplecucumberhtmlreporter]: https://raw.githubusercontent.com/wswebcreation/multiple-cucumber-html-reporter/HEAD/docs/images/browsers-features-overview.jpg 'Multiple Cucumber HTML Reporter sample'

### [Spec reporter](https://webdriver.io/docs/spec-reporter.html)

You can see Spec Reporter in the command line.

![alt text][specreporter]

[specreporter]: https://webdriver.io/img/spec.png 'Spec reporter sample'
