import { IComponent } from './BaseComponent';

class TextField implements IComponent {
  public action(selectors: any, value: string | number) {
    const existingValue = $(selectors.element).getValue();
    if (existingValue) {
      // remove existing value if any
      $(selectors.element).clearValue();
    }

    $(selectors.element).setValue(value);
  }
}

export { TextField };
