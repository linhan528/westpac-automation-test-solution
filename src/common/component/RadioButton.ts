import { IComponent } from './BaseComponent';

class RadioButton implements IComponent {
  public action(selectors: any, value: string) {
    // find the option by name
    const listOptions = $$(selectors.element);

    listOptions.forEach((element) => {
      const text = element.getText().toLowerCase();
      if (text === value.toLowerCase()) {
        element.click();
      }
    });
  }
}

export { RadioButton };
