import { IComponent } from './BaseComponent';

class DropdownField implements IComponent {
  public action(selectors: any, value: string) {
    // open  the list
    $(selectors.element).click();

    // find list item by name
    const listOptions = $$(selectors.list);

    listOptions.forEach((element) => {
      const text = element.getText().toLowerCase();
      if (text === value.toLowerCase()) {
        element.click();
      }
    });
  }
}

export { DropdownField };
