export interface IComponent {
  action(selectors: object, value?: string | string[] | number);
}
