class KiwisaverCalculatorPage {
  public open() {
    browser.url('/kiwisaver/calculators/kiwisaver-calculator');
  }

  public switchToFrame() {
    browser.switchToFrame($('#calculator-embed iframe'));
  }

  // submit button
  get submitButton() {
    return $('.btn-results-reveal');
  }

  // calculation result
  get calculationResult() {
    return $('.results-field-group-set');
  }

  // return all fields by selecting field wrapper css class
  get allFields() {
    return $$('.field-group-set-frame > div[class^="wpnib-field"]');
  }

  public getInputByFieldName(fieldName: string) {
    return `.wpnib-field-${fieldName.replace(/\s+/g, '-')} .control-row .control-cell input`;
  }

  public getRadioButtonByFieldName(fieldName: string) {
    return `.wpnib-field-${fieldName.replace(/\s+/g, '-')} .control-row .radio-control`;
  }

  public getDropdownListByFieldName(fieldName: string) {
    return {
      element: `.wpnib-field-${fieldName.replace(/\s+/g, '-')} .control-row .wpnib-drop-down .control`,
      list: `.wpnib-field-${fieldName.replace(/\s+/g, '-')} .control-row .wpnib-drop-down .control ul li`
    };
  }

  // current age

  get currentAgeIcon() {
    return $('.wpnib-field-current-age button');
  }

  get ageLimitInfoMessage() {
    return $('.wpnib-field-current-age .field-message');
  }

  public ageLimitInfoText(ageMin, ageMax) {
    return `This calculator has an age limit of ${ageMin} to ${ageMax} years old.`;
  }
}

export const kiwisaverCalculatorPage = new KiwisaverCalculatorPage();
