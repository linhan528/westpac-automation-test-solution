@ui
Feature: UI - KiwiSaver Retirement calculator

    Scenario Outline: User whose age is <currentAge> and <employmentStatus>
        Given I am on the kiwi saver retirement calculator page
        Then I can see the submit button is disabled
        Then I can not see the calculation result
        When I enter "current age" "<currentAge>"
        And  I select "employment status" "<employmentStatus>"
        And  I enter "annual income" "<annualIncome>"
        And  I choose "kiwisaver member contribution" "<contributesRate>"
        And  I choose "risk profile" "<riskProfile>"
        Then I can see the submit button is enabled
        And  I enter "kiwi saver balance" "<kiwiSaverBalance>"
        And  I enter "voluntary contributions" "<voluntaryContributes>"
        And  I select "voluntary contributions" "<voluntaryContributesFrequency>"
        And  I enter "savings goal" "<savingsGoal>"
        Then I can see the submit button is enabled
        Then I click on the submit button
        Then I can see the calculation result
        Examples:
            | currentAge | employmentStatus | annualIncome | contributesRate | kiwiSaverBalance | riskProfile  | voluntaryContributes | voluntaryContributesFrequency | savingsGoal |
            | 30         | employed         | 82000        | 4%              |                  | defensive    |                      |                               |             |
            | 45         | self-employed    |              |                 | 100000           | conservative | 90                   | fortnightly                   | 290000      |
            | 55         | not employed     |              |                 | 140000           | balanced     | 10                   | annually                      | 200000      |
