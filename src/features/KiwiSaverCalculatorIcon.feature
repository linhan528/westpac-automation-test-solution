@ui
Feature: UI - KiwiSaver retirement calculator icon check

    Background: Open kiwi saver retirement calculator page
        Given I am on the kiwi saver retirement calculator page

    Scenario: Verify all fields have information icon and message
        # employed status shows all field in the calculator
        And  I select "employment status" "employed"
        Then I can see all fields have information icon and message shows by clicking on an icon

    Scenario: Verify information icon and message in currage age question
        When I click on help icon in current age question
        Then I can see an information message in current age question
        And  I can see age limit is "18" to "64" years old in the information message
        When I click on help icon in current age question
        Then I can not see an information message in current age question

