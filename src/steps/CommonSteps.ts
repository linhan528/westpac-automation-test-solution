import { Given, Then, When } from 'cucumber';
import { DropdownField } from 'src/common/component/DropdownField';
import { RadioButton } from 'src/common/component/RadioButton';
import { TextField } from 'src/common/component/TextField';
import { kiwisaverCalculatorPage } from '../pages/KiwisaverCalculator.page';

Given(/^I am on the kiwi saver retirement calculator page$/, (): void => {
  kiwisaverCalculatorPage.open();
  kiwisaverCalculatorPage.switchToFrame();
});

When(/^I can see all fields have information icon and message shows by clicking on an icon$/, (): void => {
  kiwisaverCalculatorPage.allFields.forEach((element) => {
    // click on the icon
    element.$('button').click();
    // verifying message shows
    expect(element.$('.field-message').isDisplayed()).toEqual(true);
  });
});

When(/^I click on help icon in current age question/, (): void => {
  kiwisaverCalculatorPage.currentAgeIcon.click();
});

Then(/^I can see age limit is "(.*?)" to "(.*?)" years old in the information message$/, (ageMin, ageMax) => {
  expect(kiwisaverCalculatorPage.ageLimitInfoMessage.isDisplayed()).toEqual(true);
  expect(kiwisaverCalculatorPage.ageLimitInfoMessage.getText()).toEqual(
    kiwisaverCalculatorPage.ageLimitInfoText(ageMin, ageMax)
  );
});

Then(/^I (can|can not) see an information message in current age question$/, (visible) => {
  expect(kiwisaverCalculatorPage.ageLimitInfoMessage.isDisplayed()).toEqual(visible === 'can');
});

Then(/^I can see the submit button is (enabled|disabled)$/, (isEnabled) => {
  expect(kiwisaverCalculatorPage.submitButton.isEnabled()).toEqual(isEnabled === 'enabled');
});

Then(/^I click on the submit button$/, () => {
  kiwisaverCalculatorPage.submitButton.click();
});

Then(/^I (can|can not) see the calculation result$/, (visible) => {
  const result = visible === 'can';
  if (result) {
    kiwisaverCalculatorPage.calculationResult.waitForDisplayed();
  }
  expect(kiwisaverCalculatorPage.calculationResult.isDisplayed()).toEqual(result);
});

/** Text Field **/

Then(
  /^I enter "(current age|annual income|kiwi saver balance|voluntary contributions|savings goal)" "(.*?)"$/,
  (fieldName, value) => {
    if (value) new TextField().action({ element: kiwisaverCalculatorPage.getInputByFieldName(fieldName) }, value);
  }
);

/** DropdownField **/

Then(/^I select "(employment status|voluntary contributions)" "(.*?)"$/, (fieldName, value) => {
  if (value) new DropdownField().action(kiwisaverCalculatorPage.getDropdownListByFieldName(fieldName), value);
});

/** RadioButton **/

Then(/^I choose "(kiwisaver member contribution|risk profile)" "(.*?)"$/, (fieldName, value) => {
  if (value) new RadioButton().action({ element: kiwisaverCalculatorPage.getRadioButtonByFieldName(fieldName) }, value);
});
